# 12月后端培训作业
## 说明
- 本次作业的目的是引导同学们进行前期的技术学习，本月做完即可，下一步即是正式参加项目，寒假每个人都有项目。
- 风流的后台架构是nginx+uwsgi+django，用的是python语言，所以如果想做后台工作的同学要会python，我们用的是3.x版本，而且信通会有python课，提早接触也好。
## 时间要求：
12月31日验收
## 学习要求

python3的学习
- 学习python的安装并配置环境变量，能够在命令行中使用python。
- 学习python的基本语法，数据类型，知道类、包的概念，可以使用python实现基本的数据结构，算法。
- 学习python包调用思想，学习使用常用的包。
 
 Linux的基本使用
- 学会通过SSH和密码连接服务器，可使用Xshell、putty等工具或者使用系统自带shell连接。（可以通过连接腾讯云开发者实验室的服务器来练习）。
- 学习Linux的基本命令。
- 学会linux下使用命令安装软件，如python3等。

 参考教程：
 [菜鸟教程](http://www.runoob.com/python3/python3-tutorial.html)
 [廖雪峰](https://www.liaoxuefeng.com/wiki/0014316089557264a6b348958f449949df42a6d3a2e542c000)
[腾讯云开发者实验室](https://cloud.tencent.com/developer/labs/lab/10000)

>Linux推荐虚拟机（VMware,virtualbox等）
>Linux发行版本推荐Ubuntu，centos。
> 也可以网上买一个Linux服务器（阿里云，腾讯云等有学生优惠）。
> python的IDE推荐anaconda3,pycharm(也可先不用IDE)

## 基本作业要求及审查方式：
python
- 命令行下使用pip安装requests模块 (如果速度慢可以更换pip源，怎么更换可以上网查)
- 使用requests包，爬取[此网页](https://www.kingofbupt.com/blog/)并把内容保存下来

linux
- fork本项目
- 使用密码连接提交作业的服务器(服务器信息找群里要)，在账户主目录下新建以`名字_学号`为格式的个人文件夹
- 在里面放上你的学习记录(学习内容、感悟等，随便写)、python代码等,然后将你的信息按格式加在最下面一栏，发起PR

## 进阶学习提示（可选）
 - 学习SQL基本语法。
 - 学习基于python的django框架。
 - 学习nginx+uwsgi代理服务器的使用。
 - ~~学习英语。~~ 

## 完成作业的大佬

|	姓名	|	学号	|	作业地址	|	备注	|
|:---:|:---:|:---:|:---:|
|示例姓名|201x21xxxx|[芝麻开门](https://gitee.com/sicefengliu/2018_stage_operation_layout) | 此行勿删|
|朱勇钢|2017213564|[芝麻开门](https://gitee.com/zlab/first_homework) | 此行勿删|
|李弘宇|2017210432|[芝麻开门](https://gitee.com/LHY1999/fengliu_backend) | 此行勿删|
|廖子轩|2017210612|[嗝](https://gitee.com/SouJerk/fengliu_homework/tree/master/201812)|此行不删|
|黄程斌|2018522124|[芝麻开门](https://gitee.com/hcb98/hcb_backend_learning) |此行无删|
|胡天翼|2018210547|[两开花](https://gitee.com/TianYi_Hu/background_training)|此行不删|
|兰陈昕|2018210400|[芝麻开门](https://gitee.com/YXL76/december_training)|此行勿删|
|王若禹|2018210188|[咕咕咕](https://gitee.com/aristolochic/S5)|还有谁？|